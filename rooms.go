package main

import "fmt"

func main() {
	var room = "cave"
	if room == "cave" {
		fmt.Println("This is the 'cave' room.")
	} else if room == "entrance" {
		fmt.Println("This is the 'entrance' room.")
	} else if room == "mountain" {
		fmt.Println("This is the 'mountain' room.")
	} else {
		fmt.Println("Something else.")
	}
}
