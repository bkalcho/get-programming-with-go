package main

import (
	"fmt"
	"math/rand"
)

func main() {
	const (
		lowerBoundDistance = 56000000
		upperBoundDistance = 401000000
	)

	const diffDistance = upperBoundDistance - lowerBoundDistance + 1
	var num = rand.Intn(diffDistance) + lowerBoundDistance
	fmt.Println(num)

}
