// My weight loss program.
package main

import "fmt"

// main is the entrypoint function into program
func main() {
	//fmt.Print("My weight on the surface of Mars is: ")
	//fmt.Print(81 * 0.3783)
	//fmt.Print(" Kgs, and I would be ")
	//fmt.Print(100 * 365 / 687)
	//fmt.Print(" years old.")
	fmt.Printf("My weight on the surface of Mars is: %v Kgs,", 81*0.3783)
	fmt.Printf(" and I would be %v years old.\n", 100*365/687)
}
