package main

import "fmt"

func main() {
	fmt.Println("There is a sign near the entrance that reads 'No Minors'.")
	var age = 41
	var minor = age < 18
	fmt.Printf("At the %v, am I a minor? %v\n", age, minor)

	fmt.Println("Which is greater, an 'apple' or a 'banana'?")
	var greater = "apple" > "banana"
	fmt.Printf("'Apple' is greater then 'banana': %v\n", greater)
}
